package com.example.farbmixer_mvvm.ui.main

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Slider
import androidx.compose.material3.SliderDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.example.farbmixer_mvvm.ui.main.MainViewModel
import com.example.farbmixer_mvvm.ui.theme.FarbMixer_MVVMTheme
//Benutzeroberfläche

class MainActivity : ComponentActivity() {
   private val viewModel = MainViewModel() //MainViewModel wird instanziiert
    override fun onCreate(savedInstanceState: Bundle?) { //MainViewModel wird an ColorMixer übergeben
        super.onCreate(savedInstanceState)
        setContent {
            FarbMixer_MVVMTheme {
                ColorMixer(viewModel)
            }
        }
    }
}

@Composable
fun ColorMixer(vm: MainViewModel) {  //vm als Parameter übergeben
    val redIntensity = vm.redIntensity //vm statt remember
    val greenIntensity = vm.greenIntensity
    val blueIntensity = vm.blueIntensity

    Column(
        modifier = Modifier.padding(22.dp)
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(200.dp)
                .background(Color(redIntensity, greenIntensity, blueIntensity))
        )

        Spacer(modifier = Modifier.height(20.dp))
        CustomColorIntensityInput("Red", redIntensity) { vm.onRedIntensityChange(it) }
        Spacer(modifier = Modifier.height(20.dp))
        CustomColorIntensityInput("Green", greenIntensity) { vm.onGreenIntensityChange(it) }
        Spacer(modifier = Modifier.height(20.dp))
        CustomColorIntensityInput("Blue", blueIntensity) { vm.onBlueIntensityChange(it) }
    }
}

@Composable
fun CustomColorIntensityInput(
    colorName: String,
    intensity: Int,
    onIntensityChange: (Int) -> Unit
) {
    val thumbColor = when (colorName) {  // Farbe des Schiebereglers
        "Red" -> Color.Red
        "Green" -> Color.Green
        "Blue" -> Color.Blue
           else -> Color.Gray
    }
    Column(
        modifier = Modifier.padding(20.dp)
    ) {
        Text(text = "$colorName Intensity: $intensity")
        Slider(
            value = intensity.toFloat(),
            onValueChange = { onIntensityChange(it.toInt()) },
            valueRange = 0f..255f,
            steps = 255,
            colors = SliderDefaults.colors(
                thumbColor = thumbColor, // Farbe des Schiebereglers
                activeTrackColor = Color.Green, // Farbe der aktiven Spur
                inactiveTrackColor = Color.Black // Farbe der inaktiven Spur)
        )
        )
    }
}
