package com.example.farbmixer_mvvm.ui.main

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
//Programmierlogik  für die Farbmischung
class MainViewModel : ViewModel() {
val state = mutableStateOf(MainState())

    //getter für die Intensitäten
    val redIntensity get() = state.value.redIntensity
    val greenIntensity get() = state.value.greenIntensity
    val blueIntensity get() = state.value.blueIntensity

//Funktionen für die Intensitätsänderung
    fun onRedIntensityChange(value: Int) {
        state.value = state.value.copy(redIntensity = value)
    }
    fun onGreenIntensityChange(value: Int) {
        state.value = state.value.copy(greenIntensity = value)
    }
 //Funktionen für die Intensitätsänderung
    fun onBlueIntensityChange(value: Int) {
        state.value = state.value.copy(blueIntensity = value)
    }
}
