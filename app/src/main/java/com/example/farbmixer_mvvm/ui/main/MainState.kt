package com.example.farbmixer_mvvm.ui.main

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf

data class MainState(
    val redIntensity: Int = 0,
    val greenIntensity: Int = 0,
    val blueIntensity: Int = 0
)