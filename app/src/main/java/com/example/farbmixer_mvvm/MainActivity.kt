package com.example.farbmixer_mvvm

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.farbmixer_mvvm.ui.main.ColorMixer
import com.example.farbmixer_mvvm.ui.main.MainViewModel

import com.example.farbmixer_mvvm.ui.theme.FarbMixer_MVVMTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {//MainViewModel wird instanziiert und an ColorMixer übergeben
        super.onCreate(savedInstanceState)
        setContent {
            FarbMixer_MVVMTheme {
                ColorMixer(MainViewModel()) //MainViewModel wird instanziiert und an ColorMixer übergeben
            }
        }
    }
}

